from typing import Any, Dict, Union, List

from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

from statir.crud.base import CRUDBase
from statir.models.weapon import Weapon
from statir.schemas.weapon import WeaponCreate, WeaponUpdate


class CRUDWeapon(CRUDBase[Weapon, WeaponCreate, WeaponUpdate]):
    def get_multi_by_owner(
        self, db: Session, owner_id: int = 0, skip: int = 0, limit: int = 100
    ) -> List[Weapon]:
        return (
            db.query(self.model)
            .filter(Weapon.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def create(self, db: Session, *, owner_id: int, obj_in: WeaponCreate) -> Weapon:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete(self, db: Session, *, weapon_id: int):
        db_obj = db.query(Weapon).filter(Weapon.id == weapon_id).first()
        db.remove(db_obj)
        db.commit()

    def update(
        self,
        db: Session,
        *,
        db_obj: Weapon,
        obj_in: Union[WeaponUpdate, Dict[str, Any]]
    ) -> Weapon:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        return super().update(db, db_obj=db_obj, obj_in=update_data)


weapon = CRUDWeapon(Weapon)
