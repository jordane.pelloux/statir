from typing import Any, Dict, Union, List

from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

from statir.crud.base import CRUDBase
from statir.models.shoot_serie import ShootSerie
from statir.schemas.shoot_serie import ShootSerieCreate, ShootSerieUpdate


class CRUDShootSerie(CRUDBase[ShootSerie, ShootSerieCreate, ShootSerieUpdate]):
    def get_multi_by_owner(
        self, db: Session, owner_id: int = 0, skip: int = 0, limit: int = 100
    ) -> List[ShootSerie]:
        return (
            db.query(self.model)
            .filter(ShootSerie.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def create(
        self, db: Session, *, owner_id: int, obj_in: ShootSerieCreate
    ) -> ShootSerie:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete(self, db: Session, *, shoot_serie_id: int):
        db_obj = db.query(ShootSerie).filter(ShootSerie.id == shoot_serie_id).first()
        db.delete(db_obj)
        db.commit()

    def update(
        self,
        db: Session,
        *,
        db_obj: ShootSerie,
        obj_in: Union[ShootSerieUpdate, Dict[str, Any]]
    ) -> ShootSerie:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        return super().update(db, db_obj=db_obj, obj_in=update_data)


shoot_serie = CRUDShootSerie(ShootSerie)
