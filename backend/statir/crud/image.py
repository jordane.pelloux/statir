from typing import Any, Dict, Union, List

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from statir.crud.base import CRUDBase
from statir.models.image import Image
from statir.schemas.image import ImageCreate, ImageUpdate


class CRUDImage(CRUDBase[Image, ImageCreate, ImageUpdate]):
    def get_multi_by_owner(
        self, db: Session, shootexercice_id: int = 0, skip: int = 0, limit: int = 100
    ) -> List[Image]:
        return (
            db.query(self.model)
            .filter(Image.shootexercice_id == shootexercice_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def create(
        self, db: Session, *, shootexercice_id: int, obj_in: ImageCreate
    ) -> Image:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete(self, db: Session, *, image_id: int):
        db_obj = db.query(Image).filter(Image.id == image_id).first()
        db.remove(db_obj)
        db.commit()

    def update(
        self, db: Session, *, db_obj: Image, obj_in: Union[ImageUpdate, Dict[str, Any]]
    ) -> Image:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        return super().update(db, db_obj=db_obj, obj_in=update_data)


image = CRUDImage(Image)
