from .user import user
from .ammunition import ammunition
from .image import image
from .shoot_exercice import shoot_exercice
from .shoot_serie import shoot_serie
from .weapon import weapon
