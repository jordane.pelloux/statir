from typing import Any, Dict, Union, List

from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

from statir.crud.base import CRUDBase
from statir.models.shoot_exercice import ShootExercice
from statir.schemas.shoot_exercice import ShootExerciceCreate, ShootExerciceUpdate


class CRUDShootExercice(
    CRUDBase[ShootExercice, ShootExerciceCreate, ShootExerciceUpdate]
):
    def get_multi_by_owner(
        self, db: Session, shootserie_id: int = 0, skip: int = 0, limit: int = 100
    ) -> List[ShootExercice]:
        return (
            db.query(self.model)
            .filter(ShootExercice.shootserie_id == shootserie_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def create(self, db: Session, *, obj_in: ShootExerciceCreate) -> ShootExercice:
        obj_in_data = jsonable_encoder(obj_in)
        obj_in_data.pop("images", None)
        db_obj = self.model(**obj_in_data)

        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def delete(self, db: Session, *, shootexercice_id: int):
        db_obj = (
            db.query(ShootExercice)
            .filter(ShootExercice.id == shootexercice_id)
            .first()
        )
        db.remove(db_obj)
        db.commit()

    def update(
        self,
        db: Session,
        *,
        db_obj: ShootExercice,
        obj_in: Union[ShootExerciceUpdate, Dict[str, Any]]
    ) -> ShootExercice:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        return super().update(db, db_obj=db_obj, obj_in=update_data)


shoot_exercice = CRUDShootExercice(ShootExercice)
