# Import all the models, so that Base has them before being
# imported by Alembic
from statir.db.base_class import Base  # noqa
from statir.models.user import User  # noqa
from statir.models.ammunition import Ammunition  # noqa
from statir.models.image import Image  # noqa
from statir.models.shoot_exercice import ShootExercice  # noqa
from statir.models.shoot_serie import ShootSerie  # noqa
from statir.models.weapon import Weapon  # noqa
