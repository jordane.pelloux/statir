from sqlalchemy.orm import Session

from statir import crud, schemas
from statir.core.config import settings
from statir.db.base import Base  # noqa: F401

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


def init_db(db: Session) -> None:
    user = crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER)
    if not user:
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPERUSER,
            username=settings.FIRST_SUPERUSER_FULLNAME,
            password=settings.FIRST_SUPERUSER_PASSWORD,
            role="admin",
        )
        user = crud.user.create(db, obj_in=user_in)  # noqa: F841
