from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from statir.db.base_class import Base

if TYPE_CHECKING:
    from .shoot_exercice import ShootExercice  # noqa: F401


class Image(Base):
    id = Column(Integer, primary_key=True, index=True)
    filename = Column(String)
    width = Column(Integer)
    height = Column(Integer)

    shootexercice_id = Column(Integer, ForeignKey("shootexercice.id"))
    shootexercice = relationship("ShootExercice", back_populates="images")
