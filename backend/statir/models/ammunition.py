from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from statir.db.base_class import Base


class Ammunition(Base):
    id = Column(Integer, primary_key=True, index=True)
    brand = Column(String)

    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="ammunitions")
