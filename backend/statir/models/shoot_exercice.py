from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from statir.db.base_class import Base


class ShootExercice(Base):
    id = Column(Integer, primary_key=True, index=True)
    remark = Column(String)
    distance = Column(Integer)

    shootserie_id = Column(Integer, ForeignKey("shootserie.id"))
    shootserie = relationship("ShootSerie", back_populates="shootexercices")

    images = relationship("Image", back_populates="shootexercice")
