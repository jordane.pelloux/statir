from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from statir.db.base_class import Base


class Weapon(Base):
    id = Column(Integer, primary_key=True, index=True)
    caliber = Column(Integer)
    name = Column(String)

    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="weapons")
