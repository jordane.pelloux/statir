from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship

from statir.db.base_class import Base


class User(Base):
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    role = Column(String, default="user")

    shootseries = relationship("ShootSerie", back_populates="owner")
    weapons = relationship("Weapon", back_populates="owner")
    ammunitions = relationship("Ammunition", back_populates="owner")
