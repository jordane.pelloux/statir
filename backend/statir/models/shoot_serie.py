from sqlalchemy import Column, Integer, String, Date, ForeignKey
from sqlalchemy.orm import relationship

from statir.db.base_class import Base


class ShootSerie(Base):
    id = Column(Integer, primary_key=True, index=True)
    date = Column(Date)
    remark = Column(String)

    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="shootseries")

    # exercices
    shootexercices = relationship("ShootExercice", back_populates="shootserie")
