from fastapi import Depends, File, HTTPException, status, UploadFile
from fastapi.routing import APIRouter
from typing import List
from sqlalchemy.orm import Session

from pathlib import Path
from uuid import uuid4

from statir import crud, models, schemas
from statir.api import deps

import cv2
import numpy as np
import base64

THUMBNAIL_WIDTH = 300
THUMBNAIL_HEIGHT = 200
IMAGES_DIR = Path("/images/raw")
THUMBNAIL_DIR = Path("/images/thumbnails")

def create_image(
    db: Session,
    shootexercice_id: int,
    shoot_exercice_in: schemas.ShootExerciceCreate,
) -> List[schemas.Image]:
    """
    Create new shoot image.
    """

    response = []
    for image in shoot_exercice_in.images:
        filename = f"{uuid4()}.png"
        img_path = IMAGES_DIR / filename

        # remove "data:image/jpeg;base64,"

        nparr = np.fromstring(base64.b64decode(image[image.index(",")+1:]), np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        height, width = img.shape[:2]
        cv2.imwrite(str(img_path), img)
        new_image = schemas.ImageCreate(
            shootexercice_id = shootexercice_id,
            filename = filename,
            width = width,
            height = height
        )
        created_image = crud.image.create(
            db, shootexercice_id=shootexercice_id, obj_in=new_image
        )

        response.append(created_image)

    return response

