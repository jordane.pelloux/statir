from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from statir.api.api_v1.api import api_router
from statir.core.config import settings


def get_app() -> FastAPI:
    _app = FastAPI(
        title=settings.PROJECT_NAME,
        servers=[
            {"url": server, "description": server} for server in settings.SERVER_HOST
        ],
    )

    # Set all CORS enabled origins
    origins = ["*"]
    if settings.BACKEND_CORS_ORIGINS:
        origins = [settings.BACKEND_CORS_ORIGINS]

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    _app.include_router(api_router, prefix=settings.API_V1_STR)

    return _app


app = get_app()
