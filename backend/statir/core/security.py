from datetime import datetime, timedelta
from typing import Optional

from jose import jwt
from passlib.context import CryptContext

from statir.core.config import settings

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

blacklisted_token = []


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None) -> str:
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM
    )
    return encoded_jwt


def verify_expire(token: str) -> bool:
    """
    return true if token is expired
    """
    print(blacklisted_token)
    if token in blacklisted_token:
        return True
    decoded_jwt = jwt.decode(
        token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
    )
    try:
        return not (decoded_jwt.get("exp") < datetime.utcnow())
    except Exception:
        return False


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def blacklist_token(token: str):
    global blacklisted_token
    blacklisted_token += [token]
