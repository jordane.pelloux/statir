from fastapi import Depends, HTTPException, status
from fastapi.routing import APIRouter
from sqlalchemy.orm import Session
from typing import List

from statir import crud, models, schemas
from statir.api import deps

router = APIRouter()
prefix = "/serie"
tags = ["serie"]


@router.post("/", response_model=schemas.ShootSerie)
def create_shoot_serie(
    *,
    db: Session = Depends(deps.get_db),
    owner: models.User = Depends(deps.get_current_user),
    shoot_serie_in: schemas.ShootSerieCreate
) -> schemas.ShootSerie:
    """
    Create new shoot serie.
    """
    new_shoot_serie = crud.shoot_serie.create(
        db, owner_id=owner.id, obj_in=shoot_serie_in
    )

    return new_shoot_serie


@router.delete("/{shoot_serie_id}", response_model=schemas.ShootSerie)
def delete_shoot_serie(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
    shoot_serie_id: int = 0
) -> schemas.ShootSerie:
    shoot_serie = crud.shoot_serie.get(db, shoot_serie_id)
    if shoot_serie.owner_id != current_user.id or current_user.role != "admin":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="you can't do that."
        )
    return crud.shoot_serie.delete(db, shoot_serie_id=shoot_serie_id)


@router.get("/{shoot_serie_id}", response_model=schemas.ShootSerie)
def read_shoot_serie(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
    shoot_serie_id: int = 0,
) -> schemas.ShootSerie:
    """
    Get shoot_serie.
    """
    return crud.shoot_serie.get(db, id=shoot_serie_id)


@router.get("/", response_model=List[schemas.ShootSerie])
def get_all_shoot_series(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
    skip: int = 0,
    limit: int = 100,
) -> List[schemas.ShootSerie]:
    """
    Get list of shoot_series.
    """
    return crud.shoot_serie.get_multi_by_owner(
        db, owner_id=current_user.id, skip=skip, limit=limit
    )
