from fastapi import Depends, HTTPException, status
from fastapi.routing import APIRouter
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from datetime import timedelta

from statir import crud, models, schemas
from statir.api import deps
from statir.core.config import settings
from statir.core import security


router = APIRouter()
prefix = "/auth"
tags = ["auth"]


@router.post("/signin")
async def login(
    db: Session = Depends(deps.get_db), form_data: OAuth2PasswordRequestForm = Depends()
):
    user = crud.user.authenticate(
        db, email=form_data.username, password=form_data.password
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect username or password",
        )

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = security.create_access_token(
        data={"sub": user.email}, expires_delta=access_token_expires
    )
    return {
        "id": user.id,
        "username": user.username,
        "email": user.email,
        "is_active": user.is_active,
        "role": user.role,
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post("/signup")
async def signup(
    db: Session = Depends(deps.get_db), user_in: schemas.UserCreate = None
):
    """
    Create new user.
    """
    user = crud.user.get_by_email(db, email=user_in.email)
    if user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The user with this username already exists in the system.",
        )
    user = crud.user.create(db, obj_in=user_in)
    # if settings.EMAILS_ENABLED and user_in.email:
    #     send_new_account_email(
    #         email_to=user_in.email, username=user_in.email, password=user_in.password
    #     )

    user_authenticate = crud.user.authenticate(
        db, email=user_in.email, password=user_in.password
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Incorrect username or password",
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = security.create_access_token(
        data={"sub": user_authenticate.email}, expires_delta=access_token_expires
    )

    return {
        "id": user_authenticate.id,
        "username": user_authenticate.username,
        "email": user_authenticate.email,
        "is_active": user_authenticate.is_active,
        "role": user_authenticate.role,
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post("/logout")
async def logout(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
    token: str = Depends(deps.oauth2_scheme),
):
    security.blacklist_token(token)
