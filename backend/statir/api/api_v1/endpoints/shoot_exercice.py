from fastapi import Depends, HTTPException, status
from fastapi.routing import APIRouter
from sqlalchemy.orm import Session
from typing import List
from statir import helper
from statir import crud, models, schemas
from statir.api import deps

router = APIRouter()
prefix = "/exercice"
tags = ["exercice"]


@router.post("/", response_model=schemas.ShootExercice)
def create_shoot_exercice(
    *,
    db: Session = Depends(deps.get_db),
    owner: models.User = Depends(deps.get_current_user),
    shoot_exercice_in: schemas.ShootExerciceCreate
) -> schemas.ShootExercice:
    """
    Create new shoot exercice.
    """
    shoot_serie = crud.shoot_serie.get(db, id=shoot_exercice_in.shootserie_id)
    if not shoot_serie:
        raise HTTPException(
            status=status.HTTP_404_NOT_FOUND, detail="shoot serie not found."
        )
    if shoot_serie.owner_id != owner.id:
        raise HTTPException(
            status=status.HTTP_401_UNAUTHORIZED, detail="it's not yours."
        )
    new_shoot_exercice = crud.shoot_exercice.create(db, obj_in=shoot_exercice_in)

    if shoot_exercice_in.images:
         new_shoot_exercice.images = helper.image.create_image(db, new_shoot_exercice.id, shoot_exercice_in)

    return new_shoot_exercice


@router.delete("/{shoot_exercice_id}", response_model=schemas.ShootExercice)
def delete_shoot_exercice(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
    shoot_exercice_id: int = 0
) -> schemas.ShootExercice:
    shoot_exercice = crud.shoot_exercice.get(db, shoot_exercice_id)
    if shoot_exercice.owner_id != current_user.id or current_user.role != "admin":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="you can't do that."
        )
    return crud.shoot_exercice.delete(db, shoot_exercice_id=shoot_exercice_id)


@router.get("/{shoot_exercice_id}", response_model=schemas.ShootExercice)
def read_shoot_exercice(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
    shoot_exercice_id: int = 0,
) -> schemas.ShootExercice:
    """
    Get shoot_exercice.
    """
    return crud.shoot_exercice.get(db, id=shoot_exercice_id)


@router.get("/", response_model=List[schemas.ShootExercice])
def get_all_shoot_exercices(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
    skip: int = 0,
    limit: int = 100,
) -> List[schemas.ShootExercice]:
    """
    Get list of shoot_exercices.
    """
    return crud.shoot_exercice.get_multi_by_owner(
        db, owner_id=current_user.id, skip=skip, limit=limit
    )
