from fastapi import Depends, File, HTTPException, status, UploadFile
from fastapi.routing import APIRouter
from typing import List
from sqlalchemy.orm import Session

from pathlib import Path
from uuid import uuid4

from statir import crud, models, schemas
from statir.api import deps

import cv2
import numpy as np

THUMBNAIL_WIDTH = 300
THUMBNAIL_HEIGHT = 200
IMAGES_DIR = Path("/images/raw")
THUMBNAIL_DIR = Path("/images/thumbnails")

router = APIRouter()
prefix = "/image"
tags = ["image"]


@router.post(
    "/", response_model=List[schemas.Image], status_code=status.HTTP_201_CREATED
)
async def create_image(
    *,
    db: Session = Depends(deps.get_db),
    owner: models.User = Depends(deps.get_current_user),
    image_in: schemas.ImageCreate,
    images: List[UploadFile] = File(...),
) -> List[schemas.Image]:
    """
    Create new shoot image.
    """
    shoot_exercice = crud.shoot_exercice.get(db=db, id=image_in.shoot_exercice_id)
    if not shoot_exercice:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="this exercice doesn't exist."
        )
    shoot_serie = crud.shoot_serie.get(shoot_exercice.shootserie_id)
    if shoot_serie.owner_id != owner.id:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="this exercice is not yours.",
        )

    response = []
    for image in images:
        filename = f"{uuid4()}.png"
        img_path = IMAGES_DIR / filename

        nparr = np.fromstring(await image.read(), np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        height, width = img.shape[:2]
        cv2.imwrite(str(img_path), img)

        image_in.filename = filename
        image_in.width = width
        image_in.height = height

        image = crud.image.create(
            db, shootexercice_id=image_in.shoot_exercice_id, obj_in=image_in
        )

        response.append(image)

    return response


@router.delete("/{image_id}", response_model=schemas.Image)
def delete_image(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
    image_id: int = 0,
) -> schemas.Image:
    image = crud.image.get(db, image_id)
    if image.owner_id != current_user.id or current_user.role != "admin":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="you can't do that."
        )
    return crud.image.delete(db, image_id=image_id)


@router.get("/{image_id}", response_model=schemas.Image)
def read_image(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
    image_id: int = 0,
) -> schemas.Image:
    """
    Get image.
    """
    return crud.image.get(db, id=image_id)


@router.get("/", response_model=List[schemas.Image])
def get_all_images(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
    skip: int = 0,
    limit: int = 100,
) -> List[schemas.Image]:
    """
    Get list of images.
    """
    return crud.image.get_multi_by_owner(
        db, owner_id=current_user.id, skip=skip, limit=limit
    )
