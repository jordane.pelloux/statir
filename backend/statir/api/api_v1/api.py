from fastapi.routing import APIRouter

from statir.api.api_v1.endpoints import auth, image, shoot_exercice, shoot_serie, user

api_router = APIRouter()
api_router.include_router(auth.router, prefix=auth.prefix, tags=auth.tags)
api_router.include_router(image.router, prefix=image.prefix, tags=image.tags)
api_router.include_router(
    shoot_exercice.router, prefix=shoot_exercice.prefix, tags=shoot_exercice.tags
)
api_router.include_router(
    shoot_serie.router, prefix=shoot_serie.prefix, tags=shoot_serie.tags
)
api_router.include_router(user.router, prefix=user.prefix, tags=user.tags)
