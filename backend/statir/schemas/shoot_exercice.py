from typing import Optional, List

from pydantic import BaseModel

from statir.schemas import Image


# Shared properties
class ShootExerciceBase(BaseModel):
    distance: Optional[int] = None
    remark: Optional[str] = None


# Properties to receive on Exercice creation
class ShootExerciceCreate(ShootExerciceBase):
    shootserie_id: int
    images: Optional[List[str]] = []


# Properties to receive on Exercice update
class ShootExerciceUpdate(ShootExerciceBase):
    pass


# Properties shared by models stored in DB
class ShootExerciceInDBBase(ShootExerciceBase):
    id: int
    distance: int
    remark: str

    shootserie_id: int
    images: List[Image] = []

    class Config:
        orm_mode = True


# Properties to return to client
class ShootExercice(ShootExerciceInDBBase):
    class Config:
        orm_mode = True


# Properties properties stored in DB
class ShootExerciceInDB(ShootExerciceInDBBase):
    pass
