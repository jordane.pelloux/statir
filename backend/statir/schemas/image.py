from pydantic import BaseModel


# Shared properties
class ImageBase(BaseModel):
    width: int = 0
    height: int = 0


# Properties to receive on Image creation
class ImageCreate(ImageBase):
    filename: str
    shootexercice_id: int


# Properties to receive on Image update
class ImageUpdate(ImageBase):
    pass


# Properties shared by models stored in DB
class ImageInDBBase(ImageBase):
    id: int
    filename: str
    shootexercice_id: int

    class Config:
        orm_mode = True


# Properties to return to client
class Image(ImageBase):
    id: int
    shootexercice_id: int

    class Config:
        orm_mode = True


# Properties properties stored in DB
class ImageInDB(ImageInDBBase):
    pass
