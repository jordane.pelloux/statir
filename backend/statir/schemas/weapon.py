from typing import Optional

from pydantic import BaseModel


# Shared properties
class WeaponBase(BaseModel):
    caliber: Optional[int] = None
    name: Optional[str] = None


# Properties to receive on Exercice creation
class WeaponCreate(WeaponBase):
    caliber: int
    name: str


# Properties to receive on Exercice update
class WeaponUpdate(WeaponBase):
    pass


# Properties shared by models stored in DB
class WeaponInDBBase(WeaponBase):
    id: int
    owner_id: int
    caliber: int
    name: str

    class Config:
        orm_mode = True


# Properties to return to client
class Weapon(WeaponInDBBase):
    class Config:
        orm_mode = True


# Properties properties stored in DB
class WeaponInDB(WeaponInDBBase):
    pass
