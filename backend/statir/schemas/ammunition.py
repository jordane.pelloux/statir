from typing import Optional

from pydantic import BaseModel


# Shared properties
class AmmunitionBase(BaseModel):
    brand: Optional[str] = None


# Properties to receive on Exercice creation
class AmmunitionCreate(AmmunitionBase):
    brand: str


# Properties to receive on Exercice update
class AmmunitionUpdate(AmmunitionBase):
    pass


# Properties shared by models stored in DB
class AmmunitionInDBBase(AmmunitionBase):
    id: int
    brand: str

    class Config:
        orm_mode = True


# Properties to return to client
class Ammunition(AmmunitionInDBBase):
    class Config:
        orm_mode = True


# Properties properties stored in DB
class AmmunitionInDB(AmmunitionInDBBase):
    pass
