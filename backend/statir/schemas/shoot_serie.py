from typing import Optional, List

from pydantic import BaseModel
from datetime import date

from statir.schemas import ShootExercice


# Shared properties
class ShootSerieBase(BaseModel):
    date: Optional[date] = None
    remark: Optional[str] = None


# Properties to receive on Serie creation
class ShootSerieCreate(ShootSerieBase):
    date: date


# Properties to receive on Serie update
class ShootSerieUpdate(ShootSerieBase):
    pass


# Properties shared by models stored in DB
class ShootSerieInDBBase(ShootSerieBase):
    id: int
    date: date
    owner_id: int
    remark: str

    shootexercices: List[ShootExercice] = []

    class Config:
        orm_mode = True


# Properties to return to client
class ShootSerie(ShootSerieInDBBase):
    class Config:
        orm_mode = True


# Properties properties stored in DB
class ShootSerieInDB(ShootSerieInDBBase):
    pass
