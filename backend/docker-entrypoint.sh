#!/bin/sh

set -e

# Let the DB start
python -m statir.backend_pre_start
test $? != 0 && exit 255

# Run migrations
alembic upgrade head

# Create initial data in DB
python -m statir.initial_data

exec gunicorn -k uvicorn.workers.UvicornWorker --bind ':5000' statir.main:app