import random
import string
from fastapi.testclient import TestClient
from typing import Dict

from statir.core.config import settings


def random_lowercase_string() -> str:
    return "".join(random.choices(string.ascii_lowercase, k=32))


def random_email() -> str:
    return f"{random_lowercase_string()}@{random_lowercase_string()}.com"


def random_username() -> str:
    return random_lowercase_string()


def random_password() -> str:
    return "".join(
        random.choices(
            string.ascii_letters + string.punctuation,
            k=random.randrange(start=6, stop=32),
        )
    )


def get_user_token_headers(
    client: TestClient, username: str, password: str
) -> Dict[str, str]:
    login_data = {"username": username, "password": password}
    r = client.post(f"{settings.API_V1_STR}/auth/login", data=login_data)
    token = r.json()
    a_token = token["access_token"]
    headers = {"Authorization": f"Bearer {a_token}"}
    return headers


def get_superuser_token_headers(client: TestClient) -> Dict[str, str]:
    return get_user_token_headers(
        client=client,
        username=settings.FIRST_SUPERUSER,
        password=settings.FIRST_SUPERUSER_PASSWORD,
    )
