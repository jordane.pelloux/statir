from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from statir import crud
from statir.api.deps import get_db
from statir.core.config import settings
from statir.db.base import Base
from statir.main import app

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)

# &router.post("/", response_model=schemas.User)
# def create_user

# @router.delete("/{user_id}", response_model=schemas.User)
# def delete_user

# @router.get("/me", response_model=schemas.User)
# def read_user_me

# @router.get("/all", response_model=List[schemas.User])
# def get_all_users


def test_create_user():
    newUser = {
        "email": "test@test.test",
        "password": "password",
        "username": "test Test",
    }
    response = client.post(f"{settings.API_V1_STR}/user/", json=newUser)
    assert response.status_code == 200
    created_user = response.json()
    user = crud.user.get_by_email(TestingSessionLocal(), email="test@test.test")
    assert user
    assert user.email == created_user["email"]


def test_already_existed_user():
    newUser = {
        "email": "test@test.test",
        "password": "password",
        "username": "test Test",
    }
    response = client.post(f"{settings.API_V1_STR}/user/", json=newUser)
    assert response.status_code == 200

    response = client.post(f"{settings.API_V1_STR}/user/", json=newUser)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_delete_user():
    pass
