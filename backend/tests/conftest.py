import pytest

from typing import Generator, Any
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy_utils import create_database, database_exists, drop_database

from statir.api.deps import get_db
from statir.db.base import Base
from statir.db.init_db import init_db
from statir.main import get_app


SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"
engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture(autouse=True)
def app() -> Generator[FastAPI, Any, None]:
    Base.metadata.create_all(bind=engine)

    session = TestingSessionLocal()
    init_db(session)
    session.close()

    _app = get_app()
    yield _app
    Base.metadata.drop_all(engine)


@pytest.fixture(scope="session", autouse=True)
def setup_database():
    if database_exists(engine.url):
        drop_database(engine.url)
    create_database(engine.url)
    Base.metadata.create_all(bind=engine)

    session = TestingSessionLocal()
    init_db(session)
    session.close()

    yield
    drop_database(engine.url)

    client = TestClient(app)


@pytest.fixture()
def get_db_session():
    try:
        session = TestingSessionLocal()
        yield session
    finally:
        session.close()


@pytest.fixture()
def client(app: FastAPI, get_db_session: Session) -> Generator[TestClient, Any, None]:
    def override_get_db():
        try:
            yield get_db_session
        finally:
            pass

    app.dependency_overrides[get_db] = override_get_db
    with TestClient(app) as client:
        yield client
