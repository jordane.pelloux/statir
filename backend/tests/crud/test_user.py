from sqlalchemy.orm import Session

from statir import crud
from statir.schemas.user import UserCreate
from statir.core.security import verify_password

from tests.utils import random_email, random_password, random_username


# def create(self, db: Session, *, obj_in: UserCreate)
def test_create_user(get_db_session: Session) -> None:
    email = random_email()
    password = random_password()
    username = random_username()
    user_in = UserCreate(email=email, password=password, username=username)
    user = crud.user.create(get_db_session, obj_in=user_in)
    assert user.email == email
    assert hasattr(user, "hashed_password")


# def get_by_email(self, db: Session, *, email: str) -> Optional[User]:
def test_get_by_email(get_db_session: Session) -> None:
    email = random_email()
    password = random_password()
    username = random_username()
    user_in = UserCreate(email=email, password=password, username=username)
    created_user = crud.user.create(get_db_session, obj_in=user_in)

    get_user = crud.user.get_by_email(get_db_session, email=email)
    assert created_user == get_user


# def authenticate(self, db: Session, *, email: str, password: str) -> Optional[User]:)
def test_authenticate(get_db_session: Session) -> None:
    email = random_email()
    password = random_password()
    username = random_username()
    user_in = UserCreate(email=email, password=password, username=username)
    created_user = crud.user.create(get_db_session, obj_in=user_in)

    authenticated_user = crud.user.authenticate(
        get_db_session, email=email, password=password
    )
    assert authenticated_user
    assert created_user == authenticated_user


# def get_multi(self, db: Session, skip: int = 0, limit: int = 100) -> List[User]:
def test_get_multi(get_db_session: Session) -> None:
    users = [
        {
            "email": random_email(),
            "password": random_password(),
            "username": random_username(),
        }
        for x in range(10)
    ]
    created_users = []
    for user in users:
        user_in = UserCreate(**user)
        created_users += [crud.user.create(get_db_session, obj_in=user_in)]

    get_multi_users = crud.user.get_multi(get_db_session, skip=0, limit=100)
    assert len(get_multi_users) == len(created_users) + 1

    get_multi_users = crud.user.get_multi(get_db_session, skip=0, limit=5)
    assert len(get_multi_users) == 5


# def delete(self, db: Session, *, user_id: int) -> User:
def test_delete(get_db_session: Session) -> None:
    email = random_email()
    password = random_password()
    username = random_username()
    user_in = UserCreate(email=email, password=password, username=username)
    created_user = crud.user.create(get_db_session, obj_in=user_in)

    deleted_user = crud.user.delete(get_db_session, user_id=created_user.id)
    assert created_user == deleted_user
    get_deleted_user = crud.user.get_by_email(get_db_session, email=email)
    assert not get_deleted_user.is_active


# def update(self, db: Session, *, db_obj: User, obj_in: Union[UserUpdate, Dict[str, Any]]
def test_update(get_db_session: Session) -> None:
    email = random_email()
    password = random_password()
    username = random_username()
    user_in = UserCreate(email=email, password=password, username=username)
    created_user = crud.user.create(get_db_session, obj_in=user_in)

    email2 = random_email()
    updated_user = crud.user.update(
        get_db_session, db_obj=created_user, obj_in={"email": email2}
    )
    assert updated_user.id == created_user.id
    assert updated_user.email == email2

    password2 = random_password()
    updated_user = crud.user.update(
        get_db_session, db_obj=created_user, obj_in={"password": password2}
    )
    assert updated_user.id == created_user.id
    assert verify_password(password2, updated_user.hashed_password)
