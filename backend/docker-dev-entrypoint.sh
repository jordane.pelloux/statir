#!/bin/sh

set -e

HOST=${HOST:-0.0.0.0}
PORT=${PORT:-5000}
LOG_LEVEL=${LOG_LEVEL:-info}

# Let the DB start
PYTHONPATH="$PYTHONPATH:/src" python statir/backend_pre_start.py
test $? != 0 && exit 255

# Run migrations
alembic upgrade head

# Create initial data in DB
PYTHONPATH="$PYTHONPATH:/src" python statir/initial_data.py

# Start Uvicorn with live reload
exec uvicorn --reload --host $HOST --port $PORT --log-level $LOG_LEVEL statir.main:app