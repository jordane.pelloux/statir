import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCreateShootSerieComponent } from './dialog-create-shoot-serie.component';

describe('DialogCreateShootSerieComponent', () => {
  let component: DialogCreateShootSerieComponent;
  let fixture: ComponentFixture<DialogCreateShootSerieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogCreateShootSerieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCreateShootSerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
