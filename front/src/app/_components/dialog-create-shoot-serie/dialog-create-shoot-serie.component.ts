import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ShootSerieService } from 'src/app/_services/shoot-serie.service';
import { ShootSerieCreate } from 'src/app/_types/serie';

@Component({
  selector: 'app-dialog-create-shoot-serie',
  templateUrl: './dialog-create-shoot-serie.component.html',
  styleUrls: ['./dialog-create-shoot-serie.component.css']
})
export class DialogCreateShootSerieComponent implements OnInit {

  shoot_serie: ShootSerieCreate = new ShootSerieCreate();

  constructor(
    public dialogRef: MatDialogRef<DialogCreateShootSerieComponent>,
    private shootSerieService: ShootSerieService
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onAddClick(): void {
    this.shootSerieService.postSerie(this.shoot_serie).subscribe(
      data => {
        this.dialogRef.close(data);
    },
      err => {
        console.log(err);
    })
  }
}
