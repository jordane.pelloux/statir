import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogAnnotateImageComponent } from '../dialog-annotate-image/dialog-annotate-image.component';

@Component({
  selector: 'app-image-list-add-and-edit',
  templateUrl: './image-list-add-and-edit.component.html',
  styleUrls: ['./image-list-add-and-edit.component.css']
})
export class ImageListAddAndEditComponent implements OnInit {

  @Input() files: File[] = [];
  @Output() filesChange: any = new EventEmitter()

  files2: File[] = [];
  @Input() fileContent: Map<string, string> = new Map();
  @Output() fileContentChange: any = new EventEmitter< Map< string, string > >();


  // fileControl: FormControl;

  constructor(private dialog: MatDialog) {
    // this.fileControl = new FormControl(this.files);
  }

  ngOnInit(): void {
    // this.fileControl.valueChanges.subscribe((files: any) => {
    //   if (!Array.isArray(files)) {
    //     this.files = [files];
    //   } else {
    //     this.files = files;
    //   }
    //   this.onChange();
    // })
  }

  // onChange(): void {
  //   console.log("oki");
  //   console.log(this.files);
  //   this.files.forEach((v: File) => {
  //     const reader = new FileReader();
  //     reader.onload = () => {
  //       this.fileContent.set(v.name, (reader.result as string));
  //       console.log(this.fileContent);
  //     }
  //     reader.readAsDataURL(v)
  //   })
  // }

  openAnnotateDialog(image: string): void {
    // const dialogRef = this.dialog.open(DialogAnnotateImageComponent, {
    //   width: '50%'
    // });
    // let instance = dialogRef.componentInstance;
    // instance.image = image;

    // dialogRef.afterClosed().subscribe(result => {
    // });
  }

  onFileSelected() {
    const inputNode: any = document.querySelector('#file');

    console.log(inputNode.files);
    Array.prototype.forEach.call(inputNode.files, (v: File) => {
      const reader = new FileReader();
      reader.onload = () => {
        this.fileContent.set(v.name, (reader.result as string));
        this.fileContentChange.emit(this.fileContent);
        console.log(this.fileContent);
      }
      reader.readAsDataURL(v)
    })
  }

  cancelUpload() {

  }
}
