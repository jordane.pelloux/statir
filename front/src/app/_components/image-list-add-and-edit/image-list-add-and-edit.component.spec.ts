import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageListAddAndEditComponent } from './image-list-add-and-edit.component';

describe('ImageListAddAndEditComponent', () => {
  let component: ImageListAddAndEditComponent;
  let fixture: ComponentFixture<ImageListAddAndEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageListAddAndEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageListAddAndEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
