import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dialog-annotate-image',
  templateUrl: './dialog-annotate-image.component.html',
  styleUrls: ['./dialog-annotate-image.component.css']
})
export class DialogAnnotateImageComponent implements OnInit {

  @Input() image: string = '';
  @Output() features: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
