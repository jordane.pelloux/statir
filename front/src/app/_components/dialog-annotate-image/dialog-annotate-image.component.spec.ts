import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAnnotateImageComponent } from './dialog-annotate-image.component';

describe('DialogAnnotateImageComponent', () => {
  let component: DialogAnnotateImageComponent;
  let fixture: ComponentFixture<DialogAnnotateImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogAnnotateImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAnnotateImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
