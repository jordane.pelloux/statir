import { Component } from '@angular/core';
import { ShootSerieService } from 'src/app/_services/shoot-serie.service';
import { ShootSerie } from 'src/app/_types/serie';
import { MatDialog } from '@angular/material/dialog';
import { DialogCreateShootSerieComponent } from '../../_components/dialog-create-shoot-serie/dialog-create-shoot-serie.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  /** Based on the screen size, switch from standard to one column per row */
  series: Array<ShootSerie> = []

  constructor(
    private serieService: ShootSerieService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.refresh();
  }

  openCreateDialog(): void {
    const dialogRef = this.dialog.open(DialogCreateShootSerieComponent, {
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['/serie', result.id])
    });
  }

  onDeleteSerieClick(serie_id: number): void {
    console.log(serie_id)
    this.serieService.deleteSerie(serie_id).subscribe(
      data => {
        this.refresh();
      },
      err => {
        console.log(err);
      }
    );
  }

  refresh(): void {
    this.serieService.getSeries().subscribe(
      data => {
        this.series = <ShootSerie[]>(data);
      },
      err => {
        console.log(err);
      }
    );
  }
}
