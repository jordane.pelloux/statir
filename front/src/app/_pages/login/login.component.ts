import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from "@angular/router";
import { AuthService } from '../../_services/auth.service';
import { TokenStorageService } from '../../_services/token-storage.service';
import { UserLogin } from '../../_types/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    email: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  role: string = '';

  constructor(private authService: AuthService, private tokenStorageService: TokenStorageService, private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorageService.getToken()) {
      this.isLoggedIn = true;
      this.role = this.tokenStorageService.getUser().role!;
    }
  }

  onSubmit(): void{
    const { username, password } = this.form;
    var user: UserLogin = new UserLogin();
    user['username'] = username;
    user['password'] = password;

    this.authService.login(user).subscribe(
      data => {
        this.tokenStorageService.saveToken(data.access_token);
        this.tokenStorageService.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.role = this.tokenStorageService.getUser().role!;

        let redirectUrl = '/';
        if (this.authService.redirectUrl && this.authService.redirectUrl.length > 0) {
          redirectUrl = this.authService.redirectUrl
          this.authService.redirectUrl = "";
        }
        // Set our navigation extras object
        // that passes on our global query params and fragment
        const navigationExtras: NavigationExtras = {
          queryParamsHandling: 'preserve',
          preserveFragment: true
        };

        // Redirect the user
        this.router.navigate([redirectUrl], navigationExtras).then(() => {
          window.location.reload();
        });
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    )
  }
}
