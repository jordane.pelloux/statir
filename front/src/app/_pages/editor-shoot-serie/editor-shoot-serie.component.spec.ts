import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorShootSerieComponent } from './editor-shoot-serie.component';

describe('EditorShootSerieComponent', () => {
  let component: EditorShootSerieComponent;
  let fixture: ComponentFixture<EditorShootSerieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorShootSerieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorShootSerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
