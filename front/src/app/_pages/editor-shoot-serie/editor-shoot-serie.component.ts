import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShootExerciceService } from 'src/app/_services/shoot-exercice.service';
import { ShootSerieService } from 'src/app/_services/shoot-serie.service';
import { ShootExercice, ShootExerciceCreate } from 'src/app/_types/exercice';
import { ShootSerie } from 'src/app/_types/serie';

@Component({
  selector: 'app-editor-shoot-serie',
  templateUrl: './editor-shoot-serie.component.html',
  styleUrls: ['./editor-shoot-serie.component.css']
})
export class EditorShootSerieComponent implements OnInit {
  shoot_serie: ShootSerie = new ShootSerie();
  shoot_exercices: ShootExercice[] = [];
  new_shoot_exercice: ShootExerciceCreate = new ShootExerciceCreate();
  newImages: Map<string, string> = new Map<string, string>();

  constructor(private shootSerieService: ShootSerieService,
    private shootExerciceService: ShootExerciceService,
    private route: ActivatedRoute) {
    }

  ngOnInit(): void {
    this.refresh();
  }

  onAddExerciceClick(): void {
    this.new_shoot_exercice.shootserie_id = this.shoot_serie.id;
    this.new_shoot_exercice.images = Array.from(this.newImages.values());
    console.log(this.new_shoot_exercice);
    this.shootExerciceService.postExercice(this.new_shoot_exercice).subscribe(
      data => {
        console.log(data);
        this.refresh();
      },
      err => {
        console.log(err)
      }
    )
    this.new_shoot_exercice = new ShootExerciceCreate();
  }

  refresh(): void {
    let id = -1;
    this.route.params.subscribe(params => {
      id = params['id'];
    })
    if (id <= 0) {
      console.log("ca bugge!")
    }
    this.shootSerieService.getSerie(id).subscribe(
      data => {
        console.log(data)
        this.shoot_serie = data;
        this.shoot_exercices = this.shoot_serie.shootexercices;
        console.log(this.shoot_exercices);
      },
      err => {
        console.log(err);
      }
    )
  }
}
