import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { UserManagementService } from '../../_services/admin/user-management.service';
import { UserService } from '../../_services/user.service';
import { User } from '../../_types/user';

@Component({
  selector: 'app-admin-board',
  templateUrl: './admin-board.component.html',
  styleUrls: ['./admin-board.component.css']
})
export class AdminBoardComponent implements OnInit {

  displayedColumns: string[] = ['id', 'email', 'username', 'is_active', 'role', 'delete_user'];
  userList?: User[];

  constructor(private userManagementService: UserManagementService) { }

  ngOnInit(): void {
    this.refresh();
  }

  onUserDelete(userId: number): void {
    this.userManagementService.deleteUser(userId).subscribe(
      data => {
        this.refresh();
      },
      err => {
        console.log(JSON.parse(err.error).message);
      }
    )
  }

  refresh(): void {
    this.userManagementService.getUsers().subscribe(
      data => {
          this.userList = <User[]>JSON.parse(data);
      },
      err => {
        console.log(JSON.parse(err.error).message);
      }
    );
  }
}
