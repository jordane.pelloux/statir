import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from "@angular/router";
import { AuthService } from '../../_services/auth.service';
import { TokenStorageService } from '../../_services/token-storage.service';
import { UserCreate } from '../../_types/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {
    username: null,
    email: null,
    password: null,
    confirm_password: ''
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService, private tokenStorageService: TokenStorageService, private router: Router) { }

  ngOnInit(): void { }

  onSubmit(): void {
    const { username, email, password } = this.form;
    var newUser: UserCreate = new UserCreate();
    newUser['username'] = username;
    newUser['email'] = email;
    newUser['password'] = password;
    this.authService.register(newUser).subscribe(
      data => {
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.tokenStorageService.saveToken(data.access_token);
        this.tokenStorageService.saveUser(data);

        let redirectUrl = '/';
        if (this.authService.redirectUrl && this.authService.redirectUrl.length > 0) {
          redirectUrl = this.authService.redirectUrl
          this.authService.redirectUrl = "";
        }
        // Set our navigation extras object
        // that passes on our global query params and fragment
        const navigationExtras: NavigationExtras = {
          queryParamsHandling: 'preserve',
          preserveFragment: true
        };

        // Redirect the user
        this.router.navigate([redirectUrl], navigationExtras).then(() => {
          window.location.reload();
        });

      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
