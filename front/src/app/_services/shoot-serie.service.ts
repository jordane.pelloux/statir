import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShootSerie, ShootSerieCreate } from '../_types/serie';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class ShootSerieService {
  protected API_URL = AppConfigService.settings.apiServer.link1 + 'api/v1/serie/';

  constructor(private http: HttpClient) { }

  getSeries(skip: number = 0, limit: number = 100): Observable<any> {
    let params = new HttpParams({fromObject: {'skip': skip, 'limit': limit}})
    return this.http.get(this.API_URL, { params: params }
    )
  }

  getSerie(id: number = 0): Observable<ShootSerie> {
    return this.http.get<ShootSerie>(this.API_URL + id)
  }

  postSerie(newSerie: ShootSerieCreate): Observable<ShootSerie> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ShootSerie>(this.API_URL, JSON.stringify({ "date": `${newSerie.date.getFullYear()}-${newSerie.date.getMonth()+1}-${newSerie.date.getDate()}`, "remark": newSerie.remark}), {headers: headers})
  }

  deleteSerie(serie_id: number): Observable<any> {
    return this.http.delete(this.API_URL + serie_id)
  }
}
