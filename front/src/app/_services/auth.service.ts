import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormatWidth } from '@angular/common';
import { AppConfigService } from './app-config.service';
import { UserCreate, UserLogin } from '../_types/user';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  protected AUTH_API = AppConfigService.settings.apiServer.link1 + 'api/v1/auth/';

  isLoggedIn = false;
  // store the URL so we can redirect after logging in
  redirectUrl: string | null = null;

  constructor(private http: HttpClient) { }

  login(user: UserLogin): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    const body = new HttpParams()
      .set('username', user.username)
      .set('password', user.password)
      .set('grant_type', 'password');

    return this.http.post(this.AUTH_API + 'signin', body, {
      headers: headers,
    });
  }

  register(user: UserCreate): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.AUTH_API + 'signup', JSON.stringify(user), {headers: headers});
  }
}
