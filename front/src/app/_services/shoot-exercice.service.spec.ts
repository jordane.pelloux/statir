import { TestBed } from '@angular/core/testing';

import { ShootExerciceService } from './shoot-exercice.service';

describe('ShootExerciceService', () => {
  let service: ShootExerciceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShootExerciceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
