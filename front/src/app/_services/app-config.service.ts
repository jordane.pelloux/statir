import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


export interface IAppConfig {

    env: {
        name: string
    }

    apiServer: {
        link1: string
    }
}

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  static settings: IAppConfig;

  constructor(private http: HttpClient) { }

  load() {
    const jsonFile = `assets/config/appConfig.json`;

    return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((response : any) => {
               AppConfigService.settings = <IAppConfig>response;
               resolve();   //Return Success
            }).catch((response: any) => {
               reject(`Failed to load the config file`);
            });
        });
  }
}
