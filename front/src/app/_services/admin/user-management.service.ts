import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AppConfigService } from '../app-config.service';
import { User } from 'src/app/_types/user';
import { TokenStorageService } from '../token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  public userList: [User?] = [];
  protected API_URL = AppConfigService.settings.apiServer.link1 + 'api/v1/user/';

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { }

  getUsers(skip: number = 0, limit: number = 100): Observable<any> {
    let params = new HttpParams()
    params = params.append('skip', skip)
    params = params.append('limit', limit)
    return this.http.get(this.API_URL + 'all', { responseType: 'text', params: params });
  }

  deleteUser(userId: number): Observable<any> {
    return this.http.delete(this.API_URL + userId.toString());
  }
}
