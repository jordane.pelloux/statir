import { TestBed } from '@angular/core/testing';

import { ShootSerieService } from './shoot-serie.service';

describe('ShootSerieService', () => {
  let service: ShootSerieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShootSerieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
