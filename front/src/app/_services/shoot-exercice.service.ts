import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShootExercice, ShootExerciceCreate } from '../_types/exercice';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class ShootExerciceService {
  protected API_URL = AppConfigService.settings.apiServer.link1 + 'api/v1/exercice/';

  constructor(private http: HttpClient) { }

  getExercices(skip: number = 0, limit: number = 100): Observable<any> {
    let params = new HttpParams({ fromObject: { 'skip': skip, 'limit': limit } })
    return this.http.get(this.API_URL, { params: params }
    )
  }

  getExercice(exercice_id: number = 0): Observable<ShootExercice> {
    return this.http.get<ShootExercice>(this.API_URL + exercice_id)
  }

  postExercice(new_exercice: ShootExerciceCreate): Observable<ShootExercice> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ShootExercice>(this.API_URL, JSON.stringify(new_exercice), { headers: headers })
  }

  deleteExercice(exercice_id: number): Observable<any> {
    return this.http.delete(this.API_URL + exercice_id)
  }}
