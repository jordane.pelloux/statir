import { Injectable } from '@angular/core';
import { User } from '../_types/user';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() { }

  signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): User {
    const u = window.sessionStorage.getItem(USER_KEY);
    if (u) {
      let user: User = Object.assign(new User(), JSON.parse(u));
      return user;
    }

    return new User();
  }

  public isLoggedin(): boolean {
    const user: User = this.getUser();
    const token: string = this.getToken() as string;
    return user != null && token.length > 0;
  }
}
