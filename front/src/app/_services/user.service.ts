import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfigService } from './app-config.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  protected API_URL = AppConfigService.settings.apiServer.link1 + 'api/v1/user/';

  constructor(private http: HttpClient) { }

  getUser(): Observable<any> {
    // const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.get(this.API_URL + "me", {
      responseType: 'json',
    })
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(this.API_URL + 'admin', { responseType: 'text' });
  }
}
