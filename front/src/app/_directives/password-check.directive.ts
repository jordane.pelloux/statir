import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';


export function checkPasswordValidator(password1: string): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const check = password1.localeCompare(control.value)
    return check !== 0 ? {checkPassword: {value: control.value}} : null;
  };
}

@Directive({
  selector: '[appPasswordCheck]',
  providers: [{provide: NG_VALIDATORS, useExisting: PasswordCheckDirective, multi: true}]
})
export class PasswordCheckDirective {
  @Input('appPasswordCheck') password1 = '';

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null {
    return this.password1 ? checkPasswordValidator(this.password1)(control) : null;
  }

}
