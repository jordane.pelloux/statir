import { ShootExercice } from "./exercice";

export class ShootSerieCreate {
  'date': Date = new Date();
  'remark': string = "";

  constructor() {}
}

export class ShootSerie {
  'id': number;
  'date': string;
  'remark': string;
  'owner_id': number;
  'shootexercices': ShootExercice[];

  constructor() {}
}
