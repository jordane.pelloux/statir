export class ShootExerciceCreate {
  'shootserie_id': number;
  'distance': number = 0;
  'remark': string = "";
  'images': string[] = [];
  constructor() {}
}

export class ShootExercice {
  'id': number;
  'remark': string;
  'distance': number;
  'shootserie_id': number;
  'images': Array<object>;

  constructor() {}
}

export class ShootExerciceTest {

  constructor() {}
}
