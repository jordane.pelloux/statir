export class User {
  'email'?: string;
  'username'?: string;
  'id'?: number;
  'is_active'?: boolean;
  'role'?: string;

  constructor() {
  }
}

export class UserCreate {
  'email'?: string;
  'username'?: string;
  'password': string;

  constructor() {
  }
}

export class UserLogin {
  // 'email': string;
  'username': string;
  'password': string;

  constructor() {
  }
}
