import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminBoardComponent } from './_pages/admin-board/admin-board.component';
import { EditorShootSerieComponent } from './_pages/editor-shoot-serie/editor-shoot-serie.component';
import { HomeComponent } from './_pages/home/home.component';
import { IndexComponent } from './_pages/index/index.component';
import { LoginComponent } from './_pages/login/login.component';
import { ProfileComponent } from './_pages/profile/profile.component';
import { RegisterComponent } from './_pages/register/register.component';

import { AuthGuard } from "./_guard/auth.guard"
import { AdminGuard } from './_guard/admin.guard';

const routes: Routes = [
  { path: 'index', redirectTo: '', pathMatch: 'full'},
  { path: 'home', canActivate: [AuthGuard], component: HomeComponent },
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
  { path: 'admin', canActivate: [AuthGuard, AdminGuard], component: AdminBoardComponent },
  { path: 'serie/:id', canActivate: [AuthGuard], component: EditorShootSerieComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', component: IndexComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
