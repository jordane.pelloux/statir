import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AdminBoardComponent } from './_pages/admin-board/admin-board.component';
import { HomeComponent } from './_pages/home/home.component';
import { IndexComponent } from './_pages/index/index.component';
import { LoginComponent } from './_pages/login/login.component';
import { ProfileComponent } from './_pages/profile/profile.component';
import { RegisterComponent } from './_pages/register/register.component';
import { EditorShootSerieComponent } from './_pages/editor-shoot-serie/editor-shoot-serie.component';

import { DialogCreateShootSerieComponent } from './_components/dialog-create-shoot-serie/dialog-create-shoot-serie.component';
import { ImageListAddAndEditComponent } from './_components/image-list-add-and-edit/image-list-add-and-edit.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { PasswordCheckDirective } from './_directives/password-check.directive';
import { AppConfigService } from './_services/app-config.service';

import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatNativeDateModule } from '@angular/material/core';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { DialogAnnotateImageComponent } from './_components/dialog-annotate-image/dialog-annotate-image.component';
import { FlexLayoutModule } from '@angular/flex-layout';

export function initializeApp(appConfigService: AppConfigService) {
  return () => appConfigService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    PasswordCheckDirective,
    IndexComponent,
    AdminBoardComponent,
    DialogCreateShootSerieComponent,
    EditorShootSerieComponent,
    ImageListAddAndEditComponent,
    DialogAnnotateImageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,

    LayoutModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatListModule,
    MatExpansionModule,
    MatProgressBarModule,

    FlexLayoutModule,
  ],
  providers: [
    authInterceptorProviders,
    MatDatepickerModule,
    AppConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
