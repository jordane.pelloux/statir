import { Component } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';
import { UserService } from './_services/user.service';
import { User } from './_types/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private role: string = "";
  isLoggedIn = false;
  showAdminBoard = false;
  username?: string;

  title = 'statir';

  constructor(private tokenStorageService: TokenStorageService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user: User = this.tokenStorageService.getUser();
      this.role = user.role!;

      this.showAdminBoard = (this.role === "admin");
      this.username = user.username;
    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
